#include "stdafx.h"
#include "Ball.h"

using namespace DirectX;
using namespace Library;
using namespace std;
using namespace Microsoft::WRL;

namespace BouncingLogo
{ 
	Ball::Ball(Library::Game & game) :
		DrawableGameComponent(game), mBounds(Rectangle::Empty), viewport(mGame->Viewport())
	{
	}

	 Library::Rectangle & Ball::Bounds()
	{
		return mBounds;
	}

	 DirectX::XMFLOAT2 & Ball::Velocity()
	{
		return mVelocity;
	}

	void Ball::Initialize()
	{
		ComPtr<ID3D11Resource> textureResource;
		wstring textureName = L"Content\\Textures\\Ball.png";

		ThrowIfFailed(CreateWICTextureFromFile(mGame->Direct3DDevice(), textureName.c_str(),
			textureResource.ReleaseAndGetAddressOf(), mTexture.ReleaseAndGetAddressOf()), "CreateWICTextureFromFile() failed");

		ComPtr<ID3D11Texture2D> texture;
		ThrowIfFailed(textureResource.As(&texture), "Invalid ID3D11Resource returned from CreateWICTextureFromFile.");


		mSpriteBatch = make_shared<SpriteBatch>(mGame->Direct3DDeviceContext());

		mBounds = TextureHelper::GetTextureBounds(texture.Get());
		Point mTextureHalfSize;
		mTextureHalfSize.X = mBounds.Width / 2;
		mTextureHalfSize.Y = mBounds.Height / 2;
		
		Library::Rectangle viewportSize(static_cast<int>(viewport.TopLeftX), static_cast<int>(viewport.TopLeftY),
			static_cast<int>(viewport.Width), static_cast<int>(viewport.Height));
		Point center = viewportSize.Center();

		mBounds.X = center.X - mTextureHalfSize.X;
		mBounds.Y = center.Y - mTextureHalfSize.Y;

		mVelocity.x = 333;
		mVelocity.y = 333;
	}

	void Ball::Update(const Library::GameTime & gameTime)
	{
		float elapsedTime = gameTime.ElapsedGameTimeSeconds().count();
		
		mBounds.X += mVelocity.x * elapsedTime;
		mBounds.Y += mVelocity.y * elapsedTime;

		XMFLOAT2 position(mBounds.X, mBounds.Y);
		mBounds.X = (int)std::round<int>((int)position.x);
		mBounds.Y = (int)std::round<int>((int)position.y);

		

		if ((mBounds.Y + mBounds.Height > viewport.Height) && mVelocity.y > 0.0f)
		{
			mVelocity.y *= -1;
		}
		if (mBounds.Y <= 0 && mVelocity.y < 0.0f)
		{
			mVelocity.y *= -1;
		}


	}

	void Ball::Draw(const Library::GameTime & gameTime)
	{
		XMFLOAT2 position(mBounds.X, mBounds.Y);

		mSpriteBatch->Begin();
		mSpriteBatch->Draw(mTexture.Get(), position);
		mSpriteBatch->End();
	}

	void Ball::Reset(const Library::GameTime & gameTime,  int playerNum)
	{
		mBounds.X = (int)viewport.Width / 2;
		mBounds.Y = (int)viewport.Height / 2;
		float prevVel = mVelocity.x;
		mVelocity.y = (float)(rand() % 666 - 333);
		if (playerNum == 1)
		{
			mVelocity.x = (float)(rand() % 333 + 333);
		}
		else if (playerNum == 2)
		{
			mVelocity.x = -(float)(rand() % 333 + 333);
		}

	}


}
