#pragma once
#include "DrawableGameComponent.h"
#include "Rectangle.h"
#include <d3d11.h>
#include <DirectXMath.h>
#include <wrl.h>

namespace BouncingLogo
{
	class Ball final : public Library :: DrawableGameComponent
	{
	public:
		Ball(Library::Game& game);

	    Library::Rectangle& Bounds();
	    DirectX::XMFLOAT2& Velocity();

		virtual void Initialize() override;
		virtual void Update(const Library::GameTime& gameTime) override;
		virtual void Draw(const Library::GameTime& gameTime) override;
		void Reset(const Library::GameTime& gameTime, int playerNum);
		const D3D11_VIEWPORT viewport;

	private:
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> mTexture;
		Library::Rectangle mBounds;
		DirectX::XMFLOAT2 mVelocity;
		std::shared_ptr<DirectX::SpriteBatch> mSpriteBatch;
		
	};
}
