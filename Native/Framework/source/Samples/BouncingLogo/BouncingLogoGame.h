#pragma once

#include "Game.h"
#include "Ball.h"
#include "Paddle.h"

namespace Library
{
	class KeyboardComponent;
}

namespace BouncingLogo
{
	class Ball;
	class Paddle;

	class BouncingLogoGame : public Library::Game
	{
	public:
		BouncingLogoGame(std::function<void*()> getWindowCallback, std::function<void(SIZE&)> getRenderTargetSizeCallback);

		virtual void Initialize() override;
		virtual void Shutdown() override;
		virtual void Update(const Library::GameTime& gameTime) override;
		virtual void Draw(const Library::GameTime& gameTime) override;

		void CollisionDetection();
		int DeflectionCoefficient(int player);
		void Score(const Library::GameTime& gameTime);

		void Exit();

	private:
		static const DirectX::XMVECTORF32 BackgroundColor;

		std::shared_ptr<Library::KeyboardComponent> mKeyboard;

		std::shared_ptr<Ball> mBall;
		std::shared_ptr<Paddle> mPlayer1;
		std::shared_ptr<Paddle> mPlayer2;
		DirectX::XMFLOAT2 SDPlayer1, SDPlayer2;
		std::shared_ptr<DirectX::SpriteBatch> mSpriteBatch;
		std::shared_ptr<DirectX::SpriteFont> mSpriteFont;

		int scorep1, scorep2;
	};
}