#include "stdafx.h"
#include "Paddle.h"

using namespace Microsoft::WRL;
using namespace std;
using namespace Library;
using namespace DirectX;

namespace BouncingLogo
{
	Paddle::Paddle(Library::Game & game, int player) :
		DrawableGameComponent(game), mBounds(Rectangle::Empty)
	{
		playerNumber = player;
	}

	void Paddle::Initialize()
	{
		mKeyboard = reinterpret_cast<KeyboardComponent*>(mGame->Services().GetService(KeyboardComponent::TypeIdClass()));

		ComPtr<ID3D11Resource> textureResource;
		wstring textureName;

		switch (playerNumber)
		{
		case 1:
			textureName = L"Content\\Textures\\Paddle.png";
			break;
		case 2:
			textureName = L"Content\\Textures\\Paddle.png";
			break;
		}

		ThrowIfFailed(CreateWICTextureFromFile(mGame->Direct3DDevice(), textureName.c_str(),
			textureResource.ReleaseAndGetAddressOf(), mTexture.ReleaseAndGetAddressOf()), "CreateWICTextureFromFile() failed");

		ComPtr<ID3D11Texture2D> texture;
		ThrowIfFailed(textureResource.As(&texture), "Invalid ID3D11Resource returned from CreateWICTextureFromFile.");

		mSpriteBatch = make_shared<SpriteBatch>(mGame->Direct3DDeviceContext());

		mBounds = TextureHelper::GetTextureBounds(texture.Get());
		Point mTextureHalfSize;
		mTextureHalfSize.X = mBounds.Width / 2;
		mTextureHalfSize.Y = mBounds.Height / 2;

		auto& viewport = mGame->Viewport();
		Library::Rectangle viewportSize(static_cast<int>(viewport.TopLeftX), static_cast<int>(viewport.TopLeftY),
			static_cast<int>(viewport.Width), static_cast<int>(viewport.Height));
		Point center = viewportSize.Center();

		switch (playerNumber)
		{
		case 1:
			mBounds.X = 24 - mTextureHalfSize.X;
			floatPos.x = (float)mBounds.X;
			break;
		case 2:
			mBounds.X = (viewportSize.Right() - 24) - mTextureHalfSize.X;
			floatPos.x = (float)mBounds.X;
			break;
		}

		mBounds.Y = center.Y - mTextureHalfSize.Y;
		floatPos.y = (float)mBounds.Y;

		mVelocityY = 400;
	}

	void Paddle::Update(const Library::GameTime & gameTime)
	{
		float elapsedTime = gameTime.ElapsedGameTimeSeconds().count();

		auto& viewport = mGame->Viewport();

		MovePaddle(gameTime, viewport);

	}

	void Paddle::Draw(const Library::GameTime & gameTime)
	{
		XMFLOAT2 position((float)mBounds.X, (float)mBounds.Y);

		mSpriteBatch->Begin();
		mSpriteBatch->Draw(mTexture.Get(), position);
		mSpriteBatch->End();
	}

	void Paddle::MovePaddle(const Library::GameTime & gameTime, const D3D11_VIEWPORT vp)
	{
		float elapsedTime = gameTime.ElapsedGameTimeSeconds().count();

		switch (playerNumber)
		{
		case 1:
			if (mKeyboard->IsKeyDown(Keys::W) && mBounds.Top() >= vp.TopLeftY)
			{
				floatPos.y -= mVelocityY * elapsedTime;
				mBounds.Y = (int)std::round<int>((int)floatPos.y);
			}
			if (mKeyboard->IsKeyDown(Keys::S) && mBounds.Bottom() < vp.Height)
			{
				floatPos.y += mVelocityY * elapsedTime;
				mBounds.Y = (int)std::round<int>((int)floatPos.y);
			}
			break;
		case 2:
			if (mKeyboard->IsKeyDown(Keys::Up) && mBounds.Top() >= vp.TopLeftY)
			{
				floatPos.y -= mVelocityY * elapsedTime;
				mBounds.Y = (int)std::round<int>((int)floatPos.y);
			}
			if (mKeyboard->IsKeyDown(Keys::Down) && mBounds.Bottom() < vp.Height)
			{
				floatPos.y += mVelocityY * elapsedTime;
				mBounds.Y = (int)std::round<int>((int)floatPos.y);
			}
			break;
		}
	}

	Library::Rectangle* Paddle::Bounds()
	{
		return &mBounds;
	}

}