#include "stdafx.h"
#include "BouncingLogoGame.h"

using namespace std;
using namespace DirectX;
using namespace Library;
using namespace Microsoft::WRL;

namespace BouncingLogo
{
	const XMVECTORF32 BouncingLogoGame::BackgroundColor = Colors::Black;

	BouncingLogoGame::BouncingLogoGame(function<void*()> getWindowCallback, function<void(SIZE&)> getRenderTargetSizeCallback) :
		Game(getWindowCallback, getRenderTargetSizeCallback)
	{
	}

	void BouncingLogoGame::Initialize()
	{
		mKeyboard = make_shared<KeyboardComponent>(*this);
		mComponents.push_back(mKeyboard);
		mServices.AddService(KeyboardComponent::TypeIdClass(), mKeyboard.get());

		mBall = make_shared<Ball>(*this);
		mComponents.push_back(mBall);
		mServices.AddService(Ball::TypeIdClass(), mBall.get());
		
		mPlayer1 = make_shared<Paddle>(*this, 1);
		mComponents.push_back(mPlayer1);
		mServices.AddService(1, mPlayer1.get());

		mPlayer2 = make_shared<Paddle>(*this, 2);
		mComponents.push_back(mPlayer2);
		mServices.AddService(2, mPlayer2.get());
		
		SDPlayer1 = XMFLOAT2(Viewport().Width * 0.25f, Viewport().Height * 0.1f);
		SDPlayer2 = XMFLOAT2(Viewport().Width * 0.75f, Viewport().Height * 0.1f);
		
		mSpriteBatch = make_shared<SpriteBatch>(this->Direct3DDeviceContext());
		mSpriteFont = make_shared<SpriteFont>(this->Direct3DDevice(), L"Content\\Fonts\\Arial_14_Regular.spritefont");

		Game::Initialize();
	}

	void BouncingLogoGame::Shutdown()
	{
		Game::Shutdown();
	}

	void BouncingLogoGame::Update(const GameTime &gameTime)
	{
		if (mKeyboard->WasKeyPressedThisFrame(Keys::Escape))
		{
			Exit();
		}
		
		CollisionDetection();
		Score(gameTime);
		Game::Update(gameTime);
	}

	void BouncingLogoGame::Draw(const GameTime &gameTime)
	{
		mDirect3DDeviceContext->ClearRenderTargetView(mRenderTargetView.Get(), reinterpret_cast<const float*>(&BackgroundColor));
		mDirect3DDeviceContext->ClearDepthStencilView(mDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

		mSpriteBatch->Begin();

		wostringstream ScoreOut;
		ScoreOut << scorep1;
		mSpriteFont->DrawString(mSpriteBatch.get(), ScoreOut.str().c_str(), SDPlayer1, Colors::White, 0.0f);
		wostringstream ScoreOut2;
		ScoreOut2 << scorep2;
		mSpriteFont->DrawString(mSpriteBatch.get(), ScoreOut2.str().c_str(), SDPlayer2, Colors::White, 0.0f);

		mSpriteBatch->End();

		Game::Draw(gameTime);

		HRESULT hr = mSwapChain->Present(1, 0);

		// If the device was removed either by a disconnection or a driver upgrade, we must recreate all device resources.
		if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
		{
			HandleDeviceLost();
		}
		else
		{
			ThrowIfFailed(hr, "IDXGISwapChain::Present() failed.");
		}
	}

	void BouncingLogoGame::CollisionDetection()
	{
		if (mBall->Bounds().Intersects(*mPlayer1->Bounds()) && mBall->Velocity().x < 0)
		{
			mBall->Velocity().y = mBall->Velocity().y + DeflectionCoefficient(1);
			mBall->Velocity().x *= -1.1;

			mBall->Velocity().y *= 1.1;
		}
		if (mBall->Bounds().Intersects(*mPlayer2->Bounds()) && mBall->Velocity().x > 0)
		{
			mBall->Velocity().y = mBall->Velocity().y + DeflectionCoefficient(2);
			mBall->Velocity().x *= -1.1;

			mBall->Velocity().y *= 1.1;
		}
	}

	int BouncingLogoGame::DeflectionCoefficient(int player)
	{
		switch (player)
		{
		case 1: 
			return (mBall->Bounds().Center().Y - mPlayer1->Bounds()->Center().Y) * 4;
			break;
		case 2:
			return (mBall->Bounds().Center().Y - mPlayer2->Bounds()->Center().Y) * 4;
			break;

		}
	}

	void BouncingLogoGame::Score(const Library::GameTime & gameTime)
	{
		if ((mBall->Bounds().X > mBall->viewport.Width + 16) && mBall->Velocity().x > 0.0f)
		{
			scorep1++;
			mBall->Reset(gameTime, 1);
		}
		else if (mBall->Bounds().X <= -16 && mBall->Velocity().x < 0.0f)
		{
			scorep2++;
			mBall->Reset(gameTime, 2);
		}
	}

	void BouncingLogoGame::Exit()
	{
		PostQuitMessage(0);
	}
}