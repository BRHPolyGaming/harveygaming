#pragma once
#include "Rectangle.h"
#include "DrawableGameComponent.h"
#include <d3d11.h>
#include <DirectXMath.h>
#include <wrl.h>

namespace Library
{
	class KeyboardComponent;
}

namespace BouncingLogo
{
	class Paddle final : public Library::DrawableGameComponent
	{
	public:
		Paddle(Library::Game& game, int player);

		virtual void Initialize() override;
		virtual void Update(const Library::GameTime& gameTime) override;
		virtual void Draw(const Library::GameTime& gameTime) override;

		void MovePaddle(const Library::GameTime& gameTime, const D3D11_VIEWPORT vp);
		Library::Rectangle* Bounds();

		float mVelocityY;
		Library::Rectangle mBounds;

	private:
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> mTexture;
		std::shared_ptr<DirectX::SpriteBatch> mSpriteBatch;

		DirectX::XMFLOAT2 floatPos;

		int playerNumber;

		Library::KeyboardComponent* mKeyboard;
	};
}